var Outputs = {
  outputs: {
    label: "No. of Outputs",
    type: "textfield",
    texttype: "number",
    display: "inline",
    default: 2
  }
}

var NeuralNetwork = {
  outputs: Outputs.outputs,
  hiddenlayers: {
    label: "No. of Outputs",
    type: "textfield",
    texttype: "number",
    display: "inline",
    default: 2
  }
}

var decisonTree = {
  problemtype: {
    label: "Problem Type",
    type: "select",
    options: [{
      data: "regression",
      label: "Regression"
    }, {
      data: "classification",
      label: "Classification"
    }]
  },
  method: {
    label: "Method",
    type: "select",
    subtree: true,
    options: [{
      data: "neuralnetwork",
      label: "Neural Networks",
      decisiontrees: [NeuralNetwork]
    }, {
      data: "supportvector",
      label: "Support Vector Machine",
      decisiontrees: [Outputs]
    }]
  },
  inputs: {
    label: "No. of Inputs",
    type: "textfield",
    texttype: "number",
    display: "inline",
    default: 3
  },
  // neuralnetwork: [Inputs, Outputs, HiddenLayers, Optimizer],
}

var generatorForm = document.getElementById('generator-form')
function parseDecisionTree(decisonTree, innerHTML="", inlinedform=false) {
  for (var key in decisonTree) {
    if (decisonTree.hasOwnProperty(key)) {
      if(decisonTree[key].display == "inline" && inlinedform==false) {
        inlinedform = true
        innerHTML += '<div class="mui-form--inline">'
      } else if(inlinedform) {
        inlinedform = false
        generatorForm.innerHTML += "</div>"
      }
      if (decisonTree[key].type == "select") {
        innerHTML += '<div class="mui-select">'
        innerHTML += `<label>${decisonTree[key].label}</label>`
        innerHTML += `<select required name="${key}" id="${key}">`
        for (var i = 0;i < decisonTree[key].options.length;i+=1) {
          innerHTML += `<option value="${decisonTree[key].options[i].data}">${decisonTree[key].options[i].label}</option>`
        }
        innerHTML += '</select></div>'
      } else if (decisonTree[key].type == "textfield") {
        innerHTML += '<div class="mui-textfield">'
        innerHTML += `<label>${decisonTree[key].label}</label>`
        innerHTML += `<input type="${decisonTree[key].texttype}" value="${decisonTree[key].default}" name="${key}" required id="${key}">`
        innerHTML += '</div>'
      }
    }
  }
  return innerHTML
}

// generatorForm.innerHTML = parseDecisionTree(decisonTree)

var supportvectorInputs = document.getElementsByClassName('supportvector-input')
for (var i = 0; i < supportvectorInputs.length; i++) {
  supportvectorInputs[i].style.display = "none"
}

function registerNeuralNetworkInputs() {
  var layersSelector = document.getElementById('hiddenlayers-selector')
  var hiddenLayerInput = document.getElementById('hiddenlayers')

  hiddenLayerInput.oninput = function(e) {
    var layers = hiddenLayerInput.value
    if(layers) {
      layers = parseInt(layers)
      var innerHTML = ""
      for(var i = 0;i < layers; i+=1) {
        var layerSeletion = `<div class="mui-textfield mui-textfield--float-label neuralnetwork-input">
          <input type="number" value="5" name="hiddenlayer${i}" required id="hiddenlayer${i}">
          <label>${i+1}</label>
        </div>`
        innerHTML = innerHTML + layerSeletion
      }
      layersSelector.innerHTML = innerHTML
    }
  }
}

function registerListeners(callback) {
  document.getElementById("generator-form").onsubmit = function(e) {
    var target = e.target
    var type = e.target.problemtype.value
    var method = e.target.method.value
    var inputs = e.target.inputs.value
    var outputs = e.target.outputs.value
    if (method == "neuralnetwork") {
      var hiddenlayers = e.target.hiddenlayers.value
      var optimizer = e.target.optimizer.value
      var loss_function = e.target.loss_function.value
      hiddenlayers = parseInt(hiddenlayers)

      var hiddenlayersarr = []
      for(var i = 0;i < hiddenlayers;i+=1) {
        hiddenlayersarr.push(parseInt(e.target[`hiddenlayer${i}`].value))
      }
    } else if (method == 'supportvector') {
      var kernel = e.target.kernel.value
    }
    e.preventDefault()


    inputs = parseInt(inputs)
    outputs = parseInt(outputs)
    var body = {
      method: method,
      type: type,
      optimizer: optimizer,
      inputs: inputs,
      outputs: outputs,
      hidden_layers: hiddenlayersarr,
      loss_function: loss_function,
      kernel: kernel
    }
    var options = {
      method: "POST",
      body: JSON.stringify(body)
    }
    let myRequest = new Request("https://5xpj1pu5y8.execute-api.ap-south-1.amazonaws.com/dev/generator", options)
    var loadingElement = document.getElementById('loading')
    if(loadingElement) {
      loadingElement.style.display = "block"
    }
    var monacoEditorVsElement = document.getElementsByClassName('monaco-editor vs');
    if (monacoEditorVsElement.length > 0) {
      monacoEditorVsElement[0].style.display = "none"
    }
    fetch(myRequest)
      .then(response => response.json())
      .then(callback)
  }

  var methodInput = document.getElementById('method');
  methodInput.onchange = function(e) {
    let method = e.target.value
    console.log(method)
    var elements = document.getElementsByClassName('neuralnetwork-input');
    if(method == 'neuralnetwork') {
      for(var i = 0;i < elements.length;i+=1) {
        elements[i].style.display="block"
      }
    } else {
      for(var i = 0;i < elements.length;i+=1) {
        elements[i].style.display="none"
      }
    }
    if(method == 'supportvector') {
      document.getElementById('outputs').style.display = "none";
      for (var i = 0; i < supportvectorInputs.length; i++) {
        supportvectorInputs[i].style.display = "block"
      }
    } else {
      document.getElementById('outputs').style.display = "block";
      for (var i = 0; i < supportvectorInputs.length; i++) {
        supportvectorInputs[i].style.display = "none"
      }
    }
  }
  registerNeuralNetworkInputs()
}

var width = (window.innerWidth > 0) ? window.innerWidth : screen.width;

if (width > 767) {
  require.config({ paths: { 'vs': 'https://cdnjs.cloudflare.com/ajax/libs/monaco-editor/0.10.1/min/vs/' }});

  // Before loading vs/editor/editor.main, define a global MonacoEnvironment that overwrites
  // the default worker url location (used when creating WebWorkers). The problem here is that
  // HTML5 does not allow cross-domain web workers, so we need to proxy the instantiation of
  // a web worker through a same-domain script
  window.MonacoEnvironment = {
    getWorkerUrl: function(workerId, label) {
      return '/js/monaco-editor-worker-loader-proxy.js';
    }
  };

  var editor

  require(["vs/editor/editor.main"], function () {
    editor = monaco.editor.create(document.getElementById('code-container'), {
      value: [
        'def x() {',
        '    return "Hello world!"',
        '}'
      ].join('\n'),
      language: 'python'
    });
    document.getElementById('loading').style.display = "none"
    registerListeners(function(response) {
      document.getElementById('loading').style.display = "none"
      document.getElementsByClassName('monaco-editor vs')[0].style.display = "block"
      editor.setValue(response.code)
    })
  });
} else {
  document.getElementById('loading').remove()
  registerListeners(function(response) {
    document.getElementById('code-container').innerHTML = '<pre>' + response.code + '</pre>'
  })
}
