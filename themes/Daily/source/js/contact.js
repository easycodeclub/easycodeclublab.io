// Auto resize input
function resizeInput() {
  $(this).attr('size', $(this).val().length);
}

$('input[type="text"], input[type="email"]')
  // event handler
  .keyup(resizeInput)
  // resize on page load
  .each(resizeInput);

// Adapted from georgepapadakis.me/demo/expanding-textarea.html
(function(){

var textareas = document.querySelectorAll('.expanding'),

    resize = function(t) {
      t.style.height = 'auto';
      t.style.overflow = 'hidden'; // Ensure scrollbar doesn't interfere with the true height of the text.
      t.style.height = (t.scrollHeight + t.offset ) + 'px';
      t.style.overflow = '';
    },

    attachResize = function(t) {
      if ( t ) {
        console.log('t.className',t.className);
        t.offset = !window.opera ? (t.offsetHeight - t.clientHeight) : (t.offsetHeight + parseInt(window.getComputedStyle(t, null).getPropertyValue('border-top-width')));

        resize(t);

        if ( t.addEventListener ) {
          t.addEventListener('input', function() { resize(t); });
          t.addEventListener('mouseup', function() { resize(t); }); // set height after user resize
        }

        t['attachEvent'] && t.attachEvent('onkeyup', function() { resize(t); });
      }
    };

// IE7 support
if ( !document.querySelectorAll ) {

  function getElementsByClass(searchClass,node,tag) {
    var classElements = new Array();
    node = node || document;
    tag = tag || '*';
    var els = node.getElementsByTagName(tag);
    var elsLen = els.length;
    var pattern = new RegExp("(^|\\s)"+searchClass+"(\\s|$)");
    for (i = 0, j = 0; i < elsLen; i++) {
      if ( pattern.test(els[i].className) ) {
        classElements[j] = els[i];
        j++;
      }
    }
    return classElements;
  }

  textareas = getElementsByClass('expanding');
}

for (var i = 0; i < textareas.length; i++ ) {
  attachResize(textareas[i]);
}

$('form#contact-form').on('submit', function(e) {
  e.preventDefault();
  console.log("Submitting");

  $('form#contact-form textarea').attr('disabled', true)
  $('form#contact-form input').attr('disabled', true)

  var submitDialog = jQuery('#submittedDialog')
  var btn = jQuery('#submittedDialog #closeBtn');
  var token = "637b69ec-6ca9-456f-aa7e-90ee9f30c0bf"
  var to = "priyanshujindal1995@gmail.com"
  var subject = "Message from " + window.location.hostname
  var name = $("#your-name").val()
  var from = $("#email").val()
  var message = $("#your-message").val()
  btn.on('click', function() {
    submitDialog.css('display', 'none')
  })

  Email.send(
    from,
    to,
    subject,
    message,
    {
      token: token,
      callback: function done(message) {
        submitDialog.css('display', 'none')
        $('form#contact-form textarea').val('')
        $('form#contact-form input').val('')
      }
    }
  );
  console.log("Submitted")
  submitDialog.css('display', 'block')
  $('form#contact-form textarea').attr('disabled', false)
  $('form#contact-form input').attr('disabled', false)
})

})();
