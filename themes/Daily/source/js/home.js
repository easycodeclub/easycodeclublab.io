var subscribeForm = document.getElementById('subscribe')
var subscribeSubmitted = document.getElementById('subscribeSubmitted')
subscribeForm.onsubmit = function(e) {
  var email = e.target.email.value
  e.preventDefault();
  var host = "https://api.elasticemail.com/v2/contact/add"
  var params = {
    publicAccountID: "cdc17b3c-9fee-4601-816e-5345d5a20d62",
    email: email,
    // sendActivation: false,
    publicListID: 5406,
    // sourceUrl: window.location.host,
    notifyEmail: "priyanshujindal1995@gmail.com",
    // returnUrl: 'about:blank'
  }
  var query = Object.keys(params)
    .map(k => encodeURIComponent(k) + '=' + encodeURIComponent(params[k]))
    .join('&');
  var url = host + "?" + query
  console.log(url)
  const myRequest = new Request(url, {method: 'GET'});
  subscribeSubmitted.style.display = "block"
  subscribeForm.style.display = "none";
  fetch(myRequest)
    .then(response => response.json())
    .then(data => {
      console.log(data.success)
      response = data.data
      try {
        response = response.split('<body >')[1].split('</body>')[0]
        response = response.split('<h1>')[1].split('</h1>')[0]
      } catch(e) {
        response = "Thank you, an activation email has been sent to <b>" + email + "</b>.  Please check your email to complete the sign-up process."
      }
      subscribeSubmitted.innerHTML = response
      subscribeForm.style.display = "none";
      subscribeSubmitted.style.display = "block";
    });
}
