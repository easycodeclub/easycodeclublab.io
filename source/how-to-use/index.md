---
title: How To Use EasyCode.Club?
comments: false
date: 2018-02-13 15:52:40
---

EasyCode.Club is for <i>beginners and people without a computer-science background</i>. You <b>don't</b> have to know Python or the mathematics behind Machine Learning. However, since the programs are run on your system, it is essential to have the environment set up with the necessary dependencies installed.

If your environment is not set up yet, check out our [Tutorials](/tutorials) section for instructions.

## Machine Learning at EasyCode.Club

1. Aquire data in Excel.
2. Convert to <b>requried format</b> and save as <i>.csv</i>. Index followed by inputs, followed by outputs. <b>Example:</b> [data.csv](https://raw.githubusercontent.com/easycode-club/machinelearning-generator/master/data.csv)
3. Set model Parameters
4. Generate code
5. Modify and Download code!
