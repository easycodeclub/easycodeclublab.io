---
title: Tutorials
comments: false
date: 2018-02-13 15:50:07
---
# Installing Python

We recommend using [MiniConda](https://conda.io/miniconda.html) for installing and managing the Python environment.

# Installing libraries for Machine Learning

```bash
$ conda install keras pandas scikit-learn
```

# Installing the Jupyter Notebook

```bash
$ conda install jupyter
```


# Installing other major libraries

For plotting graphs

```bash
$ conda install matplotlib
```

# Other useful resources:

<b> Python </b>

1. [Sentdex - Youtube](https://www.youtube.com/sentdex)
2. [Learn python the hard way](https://learnpythonthehardway.org)

<b> Machine Learning </b>

1. [Machine learning by Andrew NG - Coursera](https://www.coursera.org/learn/machine-learning)
2. [Elements of Statistical Learning pdf](https://web.stanford.edu/~hastie/Papers/ESLII.pdf)
3. [Neural Networks and Deep Learing](http://neuralnetworksanddeeplearning.com/)
