---
title:   
date:
comments: false
---


# The Team

EasyCode.Club is an initiative by two engineering undergraduate students from VIT University.

<!-- md profile.html -->

# The Easy Code Project

This project uses [Keras](https://keras.io/) along with [Tensorflow](https://tensorflow.org) backend for Neural Networks, [Scikit-learn](http://scikit-learn.org/) for other Machine Learning algorithms, [Pandas](https://pandas.pydata.org/) for data handling and [Matplotlib](https://matplotlib.org/) for visualizations.
