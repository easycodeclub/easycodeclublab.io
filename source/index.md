---
layout: home
title: Welcome to Easy Code Club
comments: false
showDate: false
photos:
  - /icons/logo.svg
---

# An easy to use Code Generator!

1. Decide Machine Learning algorithm parameters
2. Select the parameters
3. Download code!

This simple!
